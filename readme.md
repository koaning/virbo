# virbo 

## project setup 

```
! wget https://s3.amazonaws.com/dl4j-distribution/GoogleNews-vectors-negative300.bin.gz
! gunzip GoogleNews-vectors-negative300.bin
```

## installation 

Install `virbo` in the virtual environment via:

```bash
$ pip install --editable .
```
