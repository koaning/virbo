import numpy as np
from scipy.spatial.distance import cosine, cdist


def predicate(lexeme):
    return lexeme.prob >= -15 and lexeme.is_alpha and lexeme.is_lower


def scaler(vec, unto):
    return vec.dot(unto) / unto.dot(unto)


def project(vec, unto):
    return scaler(vec, unto) * unto


def orthogonal(orig, away):
    return orig - project(orig, away)


class WordProjector:
    def __init__(self, nlp, axis1, axis2):
        self.nlp = nlp
        self.axis = nlp(axis1).vector - nlp(axis2).vector

    def check_diff(self, word_a, word_b, alignment=True, verbose=False):
        vec_a = self.nlp(word_a)[0].vector
        vec_b = self.nlp(word_b)[0].vector
        if not alignment:
            return cosine(vec_a, vec_b)

        a_new = orthogonal(vec_a, self.axis)
        b_new = orthogonal(vec_b, self.axis)
        if verbose:
            print(f"dist before: {cosine(vec_a, vec_b)} after:{cosine(a_new, b_new)}")
            print(f"word_a movement: {cosine(vec_a, a_new)} word_b_movement:{cosine(vec_b, b_new)}")
        return cosine(a_new, b_new)

    def calc_neighbors(self, word, n=10, alignment=True):
        word_vector = self.nlp(word).vector
        func = orthogonal if alignment else lambda v1, v2: v1
        tokens_to_consider = [t for t in self.nlp.vocab if predicate(t)]
        # first we calculate a distance matrix
        mat = np.array([func(i.vector, self.axis) for i in tokens_to_consider])
        distances = cdist(mat, [func(word_vector, self.axis)], "cosine")
        # next we select the ones with the smallest distance
        idx = distances.argsort(axis=0)
        result = []
        for i, t in enumerate(tokens_to_consider):
            if i in idx[:n + 1]:
                if t.text != word:
                    v1 = func(t.vector, self.axis)
                    v2 = func(word_vector, self.axis)
                    dist = cosine(v1, v2)
                    result.append([t.text, dist])
        return sorted(result, key=lambda d: d[1])
