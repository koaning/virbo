import random
import logging

import click

logging.basicConfig(
    level=logging.DEBUG,
    format='%(asctime)s [%(filename)s:%(funcName)s:%(lineno)d] %(levelname)s - %(message)s',
)
logger = logging.getLogger(__name__)


@click.group()
def cli():
    logger.debug("running roll command")
    pass


@click.command()
@click.option('--sides', default=6, help='how many sides your ')
@click.option('--times', default=1, help='name of the user')
def roll(sides, times):
    logger.debug("running roll command")
    eyes = sum([random.randint(1, sides) for _ in range(times)])
    logging.debug(f"we rolled {times} dice and got {eyes} eyes")


@click.command()
@click.option('--name', default=6, help='how many sides your ')
def hello(name):
    logger.debug("running hello command")
    print(f"hello {name}")


cli.add_command(roll)
cli.add_command(hello)


if __name__ == "__main__":
    cli()
